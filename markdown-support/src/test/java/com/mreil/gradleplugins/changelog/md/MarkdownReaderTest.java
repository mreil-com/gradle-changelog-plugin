package com.mreil.gradleplugins.changelog.md;

import com.vladsch.flexmark.formatter.Formatter;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

public class MarkdownReaderTest {

    @Test
    public void testInitial() throws IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream("mdfiles/initial-changelog.md");

        MarkdownReader markdownReader = new MarkdownReader(file);
        Document node = markdownReader.getDocument();
        print(node, 0);

        markdownReader.replaceUnchangedWithVersion("1.2.3");

        Formatter formatter = Formatter.builder().build();
        String render = formatter.render(markdownReader.getDocument());
        System.out.println(render);

        // TODO assert
    }

    @Test
    public void testSecond() throws IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream("mdfiles/changelog-after-first-release.md");

        MarkdownReader markdownReader = new MarkdownReader(file);
        Document node = markdownReader.getDocument();
        print(node, 0);

        markdownReader.replaceUnchangedWithVersion("1.2.4");

        Formatter formatter = Formatter.builder().build();
        String render = formatter.render(markdownReader.getDocument());
        System.out.println(render);

        // TODO assert
    }

    @Test
    public void testThird() throws IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream("mdfiles/changelog-after-second-release.md");

        MarkdownReader markdownReader = new MarkdownReader(file);
        Document node = markdownReader.getDocument();
        print(node, 0);

        markdownReader.replaceUnchangedWithVersion("1.2.5");

        Formatter formatter = Formatter.builder().build();
        String render = formatter.render(markdownReader.getDocument());
        System.out.println(render);

        // TODO assert
    }

    private void print(Document node, int indent) {
        print(node.getFirstChild(), 0);
    }

    private void print(Node node, int indent) {
        if (node == null) {
            return;
        }

        for (int i = 0; i < indent; i++)
            System.out.print("\t");
        System.out.println(node);

        Node child = node.getFirstChild();
        print(child, indent + 1);

        Node next = node.getNext();
        print(next, indent);
    }

}
