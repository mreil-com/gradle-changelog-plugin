package com.mreil.gradleplugins.changelog.md;

import com.vladsch.flexmark.ast.Heading;
import com.vladsch.flexmark.ast.LinkRef;
import com.vladsch.flexmark.ast.Reference;
import com.vladsch.flexmark.ast.Text;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.ast.NodeVisitor;
import com.vladsch.flexmark.util.ast.VisitHandler;
import com.vladsch.flexmark.util.sequence.BasedSequence;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import static com.vladsch.flexmark.util.Utils.asBased;

public class MarkdownReader {

    private static final String UNRELEASED_REF_TEXT = "Unreleased";
    private final Document node;

    public MarkdownReader(InputStream inputStream) throws IOException {
        Parser parser = Parser.builder().build();
        node = parser.parseReader(new InputStreamReader(inputStream));
    }

    public Document getDocument() {
        return node;
    }

    public void replaceUnchangedWithVersion(String version) {
        new NodeVisitor(
                new UnreleasedLinkChangeHandler(version),
                new UnreleasedReferenceChangeHandler(version)
        ).visit(node);
    }

    private static class UnreleasedLinkChangeHandler extends VisitHandler<LinkRef> {
        UnreleasedLinkChangeHandler(String version) {
            super(LinkRef.class, node -> {
                if (node.getReference().matchChars(UNRELEASED_REF_TEXT)) {
                    insertNewUnreleasedHeading(node);
                    changeHeadingToVersion(node, version);
                }
            });
        }

        private static void insertNewUnreleasedHeading(LinkRef node) {
            Heading newUnreleased = new Heading(asBased("ble"));
            newUnreleased.setLevel(2);
            newUnreleased.setOpeningMarker(asBased("##"));
            newUnreleased.setClosingMarker(BasedSequence.NULL);

            LinkRef newUnreleasedRef = new LinkRef();
            newUnreleasedRef.setReference(asBased(UNRELEASED_REF_TEXT));
            newUnreleasedRef.setReferenceOpeningMarker(asBased("["));
            newUnreleasedRef.setReferenceClosingMarker(asBased("]"));

            Text newUnreleasedText = new Text(asBased(UNRELEASED_REF_TEXT));

            newUnreleased.appendChild(newUnreleasedRef);
            newUnreleasedRef.appendChild(newUnreleasedText);

            node.getParent().insertBefore(newUnreleased);
        }

        private static void changeHeadingToVersion(LinkRef node, String version) {
            String date = new SimpleDateFormat("yyyy-MM-dd").format(
                    new Date(ZonedDateTime.now(ZoneOffset.UTC).toInstant().toEpochMilli())
            );
            node.setReference(asBased(version));
            node.getParent().appendChild(new Text(asBased(" - " + date)));
        }
    }

    private static class UnreleasedReferenceChangeHandler extends VisitHandler<Reference> {
        UnreleasedReferenceChangeHandler(String version) {
            super(Reference.class, ref -> {
                if (ref.getReference().matchChars(UNRELEASED_REF_TEXT)) {

                    Node next = ref.getNext();
                    if (next != null && next.isOrDescendantOfType(Reference.class)) {
                        Reference previousReference = ((Reference) next);

                        // There is already one link there, get the tag
                        final String previousVersion = previousReference.getReference().toString();
                        ref.setChars(asBased("[" + version + "]: ../../branches/compare/v" + version + "..v" + previousVersion + "#diff"));
                    } else {
                        // Just link to the tag, this is the first version
                        ref.setChars(asBased("[" + version + "]: ../../src/v" + version));
                    }

                    // Insert new Unreleased ref
                    BasedSequence label = asBased("[Unreleased]: ../");
                    Reference sibling = new Reference(label, label, null);
                    ref.insertBefore(sibling);
                }
            });
        }
    }
}
